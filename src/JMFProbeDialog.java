import javax.media.CannotRealizeException;
import javax.media.Manager;
import javax.media.NoPlayerException;
import javax.media.Player;
import javax.sound.sampled.*;
import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import static javax.sound.sampled.AudioFormat.Encoding.PCM_SIGNED;
import static javax.sound.sampled.AudioSystem.getAudioInputStream;

public class JMFProbeDialog extends JFrame {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JButton button1;
    private JButton googleButton;
    private JButton googleSimpleButton;
    private JButton simpleStopButton;
    private Clip clip;

    public JMFProbeDialog() {
        setContentPane(contentPane);
        //setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        button1.addActionListener(e -> {
            URL sourceURL = null;
            try {
                File file = getFile();
                if (file != null) {
                    sourceURL = file.toURI().toURL();
                }
            } catch (MalformedURLException e1) {
                e1.printStackTrace();
            }

            Player player = null;
            try {
                if (sourceURL != null) {
                    player = Manager.createRealizedPlayer(sourceURL);
                    player.start();
                }
            } catch (IOException | NoPlayerException | CannotRealizeException e1) {
                e1.printStackTrace();
            }
        });
        googleButton.addActionListener(e -> {
            File file = getFile();
            if (file != null) {
                try (final AudioInputStream in = getAudioInputStream(file)) {

                    final AudioFormat outFormat = getOutFormat(in.getFormat());
                    final DataLine.Info info = new DataLine.Info(SourceDataLine.class, outFormat);

                    try (final SourceDataLine line =
                                 (SourceDataLine) AudioSystem.getLine(info)) {

                        if (line != null) {
                            line.open(outFormat);
                            line.start();
                            stream(getAudioInputStream(outFormat, in), line);
                            line.drain();
                            line.stop();
                        }
                    }

                } catch (UnsupportedAudioFileException
                        | LineUnavailableException
                        | IOException e1) {
                    throw new IllegalStateException(e1);
                }
            }
        });
        googleSimpleButton.addActionListener(e -> {
            File soundFile = getFile();

            if (soundFile != null) {
                try {
                    System.out.println("format: " + AudioSystem.getAudioFileFormat(soundFile));

                    //Получаем AudioInputStream
                    //Вот тут могут полететь IOException и UnsupportedAudioFileException
                    AudioInputStream ais = getAudioInputStream(soundFile);

                    final AudioFormat outFormat = getOutFormat(ais.getFormat());

                    //Получаем реализацию интерфейса Clip
                    //Может выкинуть LineUnavailableException
                    clip = AudioSystem.getClip();
                    clip.addLineListener(event -> System.out.println(event.getType()));

                    //Загружаем наш звуковой поток в Clip
                    //Может выкинуть IOException и LineUnavailableException
                    clip.open(getAudioInputStream(outFormat, ais));

                    clip.setFramePosition(0); //устанавливаем указатель на старт
                    clip.start(); //Поехали!!!

                    //Если не запущено других потоков, то стоит подождать, пока клип не закончится
                    //В GUI-приложениях следующие 3 строчки не понадобятся
                /*Thread.sleep(clip.getMicrosecondLength()/1000);
                clip.stop(); //Останавливаем
                clip.close(); //Закрываем*/
                } catch (IOException | UnsupportedAudioFileException | LineUnavailableException exc) {
                    exc.printStackTrace();
                } /*catch (InterruptedException ignored) {}*/
            }
        });
        simpleStopButton.addActionListener(e -> {
            if (clip != null && clip.isRunning()) {
                clip.stop();
                clip.close();
            }
        });
    }

    public static void main(String[] args) {
        JMFProbeDialog dialog = new JMFProbeDialog();
        dialog.pack();
        dialog.setVisible(true);
        //System.exit(0);
    }

    private AudioFormat getOutFormat(AudioFormat inFormat) {
        final int ch = inFormat.getChannels();

        final float rate = inFormat.getSampleRate();
        return new AudioFormat(PCM_SIGNED, rate, 16, ch, ch * 2, rate, false);
    }

    private void stream(AudioInputStream in, SourceDataLine line)
            throws IOException {
        final byte[] buffer = new byte[4096];
        for (int n = 0; n != -1; n = in.read(buffer, 0, buffer.length)) {
            line.write(buffer, 0, n);
        }
    }

    private File getFile() {
        JFileChooser fileChooser = new JFileChooser();
        boolean isFileSelected = fileChooser.showDialog(JMFProbeDialog.this, null) == JFileChooser.APPROVE_OPTION;
        return isFileSelected ? fileChooser.getSelectedFile() : null;
    }

    private void onOK() {
// add your code here
        dispose();
    }

    private void onCancel() {
// add your code here if necessary
        dispose();
    }
}
